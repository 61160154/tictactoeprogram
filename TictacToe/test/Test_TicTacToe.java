/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertFalse;

/**
 *
 * @author Admin
 */
public class Test_TicTacToe {
    public TicTacToe ox = new TicTacToe(3, 3);
    public Test_TicTacToe() {
        
    }
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void shouldBeNotEnd_whenInputEmpty() {
        assertFalse(ox.isEnd());
    }
    
    @Test
    public void shouldBeNotEnd_whenInput2Turns(){
        ox.setBoard(1, 1, 'X');
        ox.setBoard(0, 2, 'O');
        assertFalse(ox.isEnd());
    }
    
    @Test
    public void shouldBeEnd_whenInputDraw(){
        ox.setBoard(1, 1, 'X');
        ox.setBoard(0, 0, 'O');
        ox.setBoard(0, 2, 'X');
        ox.setBoard(2, 0, 'O');
        ox.setBoard(1, 0, 'X');
        ox.setBoard(0, 1, 'O');
        ox.setBoard(2, 1, 'X');
        ox.setBoard(1, 2, 'O');
        assertTrue(ox.isEnd());
    }
    
    @Test
    public void shouldBeEnd_whenInputXInCol() {
        ox.setBoard(0, 0, 'X');
        ox.setBoard(1, 1, 'O');
        ox.setBoard(1, 0, 'X');
        ox.setBoard(0, 2, 'O');
        ox.setBoard(2, 0, 'X');  
        assertTrue(ox.isEnd());
    }
    
    @Test
    public void shouldBeEnd_whenInputXInRow() {
        ox.setBoard(1, 0, 'X');
        ox.setBoard(0, 1, 'O');
        ox.setBoard(1, 1, 'X');
        ox.setBoard(0, 2, 'O');
        ox.setBoard(1, 2, 'X');  
        assertTrue(ox.isEnd());
    }
    
    @Test
    public void shouldBeEnd_whenInputXInXCase() {
        ox.setBoard(0, 0, 'X');
        ox.setBoard(0, 1, 'O');
        ox.setBoard(1, 1, 'X');
        ox.setBoard(2, 2, 'O');
        ox.setBoard(1, 2, 'X');
        ox.setBoard(1, 0, 'O');
        ox.setBoard(0, 2, 'X');
        ox.setBoard(2, 1, 'O');
        ox.setBoard(2, 0, 'X');
        assertTrue(ox.isEnd());
    }
    
    @Test
    public void shouldBeX_whenInput0_0() {
        ox.setBoard(0, 0, 'X');

        assertEquals('X', ox.getBoardAt(0, 0));
    }
    
    @Test
    public void shouldBe4_whenInput3Turns() {

        ox.setBoard(0, 0, 'X');
        ox.setBoard(1, 1, 'O');
        ox.setBoard(1, 0, 'X');

        assertEquals(4, ox.getTurn());
    }
    
    @Test
    public void shouldBeEnd_whenInputOInRow() {
        ox.setBoard(0, 0, 'X');
        ox.setBoard(2, 0, 'O');
        ox.setBoard(1, 1, 'X');
        ox.setBoard(2, 1, 'O');
        ox.setBoard(1, 2, 'X');
        ox.setBoard(2, 2, 'O');
        assertTrue(ox.isEnd());
    }
    
        @Test
    public void shouldBeEnd_whenInputOInCol() {
        ox.setBoard(0, 1, 'X');
        ox.setBoard(0, 2, 'O');
        ox.setBoard(1, 1, 'X');
        ox.setBoard(1, 2, 'O');
        ox.setBoard(0, 0, 'X');
        ox.setBoard(2, 2, 'O');
        assertTrue(ox.isEnd());
    }
    
        @Test
        public void shouldBeO_whenInput1_0() {
        ox.setBoard(0, 0, 'X');
        ox.setBoard(1, 0, 'O');

        assertEquals('O', ox.getBoardAt(1, 0));
    }
}
