package tictactoe;

import java.util.Scanner;

public class TictacToe {

	public static Scanner kb = new Scanner(System.in);
	int xScore = 0;
	int oScore = 0;

	public static void showHeading() {
		System.out.println("!!Welcome to XO Game!!");
	}

	public static void showTable(char[][] table) {

		for (int i = 0; i < table.length; i++) {
			for (int j = 0; j < table[i].length; j++) {
				System.out.print(" " + table[i][j] + " ");
			}
			System.out.println();
			System.out.println();
		}
	}

	public static void showTurn(int turn) {
		if (turn % 2 == 1) {
			System.out.println("X Turn");
		} else {
			System.out.println("O Turn");
		}
	}

	public static char getSymbol(int turn) {
		if (turn % 2 == 1) {
			return 'X';
		}
		return 'O';

	}

	public static void input(int turn, char[][] table) {
		while (true) {
			System.out.print("Please input [1-9] : ");
			int inputNumber = kb.nextInt();
			if (inputNumber < 1 || inputNumber > 9) {
				System.out.println("Please input number 1-9 only");
				continue;
			}
			int row = 2 - (inputNumber - 1) / 3;
			int col = (inputNumber - 1) % 3;
			if (table[row][col] != 'X' && table[row][col] != 'O') {
				table[row][col] = getSymbol(turn);
				break;
			} else {
				System.out.println("Error, please input again.");
			}
		}
	}


	public static boolean isEnd(int turn, char table[][]) {
		if (checkRow(table) || checkColumn(table) || checkXcase(table)) {
			showTable(table);
			if (turn % 2 == 1) {
				System.out.println("X Win!\n");
			} else {
				System.out.println("O Win!\n");
			}
			return true;
		}
		if (turn == 9) {
			showTable(table);
			System.out.println("Draw!\n");
			return true;
		}
		return false;
	}

	public static boolean checkRow(char[][] table) {
		for (int i = 0; i < 3; i++) {
			if (table[i][0] == table[i][1] && table[i][1] == table[i][2])
				return true;
		}
		return false;
	}

	public static boolean checkColumn(char[][] table) {
		for (int i = 0; i < 3; i++) {
			if (table[0][i] == table[1][i] && table[1][i] == table[2][i])
				return true;
		}
		return false;
	}

	public static boolean checkXcase(char[][] table) {
		if (table[0][0] == table[1][1] && table[1][1] == table[2][2])
			return true;

		if (table[0][2] == table[1][1] && table[1][1] == table[2][0])
			return true;

		return false;
	}

	public static boolean isRestart(boolean end, int turn) {
		if (end) {
			System.out.print("Do you want to restart?, please input [Y, N] : ");
			char inputChar = kb.next().charAt(0);
			if (inputChar == 'Y' || inputChar == 'y') {
				return true;
			}
		}
		return false;
	}

	public static void clearTable(char[][] table) {
		for (int i = 1; i < 10; i++) {
			int row = 2 - (i - 1) / 3;
			int col = (i - 1) % 3;
			table[row][col] = (char) (i + '0');
		}

	}



	public static void main(String[] args) {
		int turn = 0;
		char table[][] = { { '7', '8', '9' }, { '4', '5', '6' }, { '1', '2', '3' } };
		showHeading();
		while (true) {
			turn = turn + 1;
			showTable(table);
			showTurn(turn);
			input(turn, table);
			boolean end = isEnd(turn, table);
			if (end) {
				boolean restart = isRestart(end, turn);
				if (restart) {
					clearTable(table);
					turn = 0;
					continue;
				}
				break;
			}
		}
	}
}