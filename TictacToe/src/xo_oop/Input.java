/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xo_oop;

import java.util.Scanner;

public class Input {
  Scanner kb = new Scanner(System.in);
  TicTacToe ox;
  
  Input(TicTacToe g) {
    ox = g;
  }

  private char getSymbol(int turn) {
    if (turn % 2 == 1) {
      return 'X';
    }
    return 'O';
  }

  void getInput() {
    while (true) {
      System.out.print("Please input [1-9] : ");
      int inputNumber = kb.nextInt();
      if (inputNumber < 1 || inputNumber > 9) {
        System.out.println("Please input number 1-9 only");
        continue;
      }
      int row = 2 - (inputNumber - 1) / 3;
      int col = (inputNumber - 1) % 3;


      if (ox.getBoardAt(row, col) != 'X' && ox.getBoardAt(row, col) != 'O') {
        char symbol = getSymbol(ox.getTurn());
        ox.setBoard(row, col, symbol);
        ox.setTurn(ox.getTurn()+1);
        break;
      } else {
        System.out.println("Error, please input again.");
      }
    }
  }
  boolean isRestart(){
      Scanner kb = new Scanner(System.in);
      
      System.out.print("Do you wanna restart? :");
      char inputRestart = kb.next().charAt(0);
      if(inputRestart == 'Y'|| inputRestart == 'y'){
          return true;
      }else{
          return false;
      }
  }




}


