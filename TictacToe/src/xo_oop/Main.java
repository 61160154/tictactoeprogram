/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xo_oop;

/**
 *
 * @author Admin
 */
public class Main {
  public static void main(String[] args) {
    TicTacToe g = new TicTacToe(3, 3);
    Display d = new Display(g);
    Input i = new Input(g);
    while(true) {
      d.showHeading();
      g.showScore();
      while(!g.isEnd()) {
        g.showTurn();
        i.getInput();
        d.printBoard();
      }
      if(i.isRestart()){
          g.restart();
      }else{
          break;
      }
      
      
    }
    
  }
}
